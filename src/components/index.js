import Layout from './layout';
import Avatar from './avatar';
import Header from './header';
import Navigation from './navigation';
import SEO from './seo';
import Footer from './footer';

export {
    Layout,
    Avatar,
    Header,
    Navigation,
    SEO,
    Footer,
};
