const links = [
    {
        name: 'gitlab',
        url: 'https://gitlab.com/marcoscarpettini',
    },
    {
        name: 'linkedin',
        url: 'https://linkedin.com/in/marco-scarpettini',
    },
    {
        name: 'google',
        url: 'mailto: marcoscarpettini@gmail.com'
    }
];

export default links;