import React from 'react';
import { Helmet } from 'react-helmet';

import Favicon from '../../images/favicon.ico';

function SEO(props) {
    return (
        <>
            <Helmet titleTemplate='%s | Marco Scarpettini'>
                <html lang="es"/>
                <title>{props.title}</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="description" content="Esta es una página de presentación y portfolio de desarrollos donde pueden encontrar información acerca de mí, de mis trabajos y de contacto." />
                <meta name="og:description" content="Esta es una página de presentación y portfolio de desarrollos donde pueden encontrar información acerca de mí, de mis trabajos y de contacto." />
                <meta name="og:title" content={props.title} />
                <meta name="author" content="Marco Scarpettini" />
                <meta name="og:type" content="website" />
                <link rel="shortcut icon" href={ Favicon } type="image/x-icon"></link>
                <link rel="icon" href={ Favicon } type="image/x-icon"></link>
            </Helmet>
        </>
    );
}

export default SEO