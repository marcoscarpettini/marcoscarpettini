import React from 'react';
import { withStyles } from '@material-ui/styles';

import styles from './style';
import AvatarPic from '../../images/letra-m.png';

function Avatar(props) {
    const {classes} = props;
    return (
        <div className={classes.container}>
            <img src={AvatarPic} className={classes.avatar} alt='avatar' />
        </div>
    )
};

export default withStyles(styles)(Avatar);