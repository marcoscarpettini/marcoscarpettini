import React from 'react';
import { withStyles } from '@material-ui/styles';
import { Scrollbars } from 'react-custom-scrollbars';
import Fade from '@material-ui/core/Fade';

import styles from './style';
import { Header, Footer } from '../';

function Layout(props) {
  const { classes, children } = props;

  return (
      <Scrollbars
        autoHide
        autoHideTimeout={1000}
        autoHideDuration={200}
        renderView={props => <div {...props} className={classes.scrollFix} />}
        className={classes.container}>
        <div className={classes.pageContainer}>
            
          <Header />
          <Fade in mountOnEnter unmountOnExit>
            <main className={classes.content}> { children } </main>
          </Fade>
          
        </div>
        <Footer />
      </Scrollbars>
  )};
  
export default withStyles(styles)(Layout);