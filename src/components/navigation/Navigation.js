import React from 'react';
import { withStyles } from '@material-ui/styles';
import { Link } from 'gatsby';
import { IconButton, Menu, MenuItem } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import styles from './style';
import options from './content';

function Navigation(props) {
    const {classes} = props;

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.container}>
            <div className={classes.displayComputer}>
                <ul style={{margin: 0,}}>
                    {options.map((link, i) => (
                    <li className={classes.menuContainer} key={i}>
                        <Link
                            to={link.to}
                            className={classes.navLink}
                            activeClassName={classes.navLinkActive}
                            exact={'true'}>
                            <span>
                                {link.text}
                            </span>
                        </Link>
                    </li>
                    ))}
                </ul>
            </div>
            <div className={classes.displayMobile}>
                <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                    <MenuIcon />
                </IconButton>
                <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                >
                    {options.map((link, i) => (
                        <MenuItem onClick={handleClose} key={i}>
                            <Link
                                to={link.to}
                                exact={'true'}
                                style={{textDecoration: 'none', decoration: 'none', color: 'black'}}
                                >{link.text}
                            </Link>
                        </MenuItem>
                    ))}
                </Menu>
            </div> 
        </div>
    );
};

export default withStyles(styles)(Navigation);