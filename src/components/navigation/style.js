const styles = () => ({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        listStyle: 'none',
    },
    menuContainer: {
        borderRadius: '.25rem',
        display: 'inline-block',
    },
    navLink: {
        border: 'none',
        display: 'inline-block',
        padding: '.5rem .5rem',
        textDecoration: 'none',
        color: 'black',
    },
    navLinkActive: {
        borderBottom: '.125rem solid black',
    },
    displayComputer: {
        display: 'none',
        '@media (min-width: 600px)' : {
            display: 'flex'
        }
    },
    displayMobile: {
        display: 'flex',
        '@media (min-width: 600px)' : {
            display: 'none'
        }
    },
});

export default styles;