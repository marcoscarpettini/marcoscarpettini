const styles = () => ({
    container: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: '.5rem auto 0',
        maxWidth: '100%',
        width: '100%',
    },
    logo: {
        border: 'none',
        float: 'left',
        transition: 'all .3s',
        textDecoration: 'none',
    },
    navigation: {
        float: 'right',
        position: 'relative',
    },
});

export default styles;