import React from 'react';
import { Link } from 'gatsby';
import { withStyles } from '@material-ui/styles';

import styles from './style';

import { Avatar, Navigation } from '../';

function Header(props) {
    const {classes} = props;
    
    return (
        <header className={classes.container} id='header'>
            <Link to='/' className={classes.logo}>
                <Avatar />
            </Link>
            <div className={classes.navigation}>
                <Navigation />
            </div>
        </header>
    )
};

export default withStyles(styles)(Header);