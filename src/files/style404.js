const styles  = () => ({
    '@global' :{
        html: {
            overflowY: 'hidden !important',
        },
        body: {
            backgroundImage: 'linear-gradient(-45deg, #5ca4da, #6daee0)',
            height: '100vh',
        }
    }
});

export default styles;