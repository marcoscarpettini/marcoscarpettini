import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Fade from '@material-ui/core/Fade';
import { withStyles } from '@material-ui/styles';

import styles from './style';
import aboutme from './content';

const ColorButton = withStyles(() => ({
    root: {
      color: '#ffffff',
      backgroundColor: '#6daee0',
      '&:hover': {
        backgroundColor: '#5ca4da',
      },
    },
}))(Button);

function Bio(props) {
    const {classes} = props;

    return (
        <div>
            <Grid container spacing={8} justify='center' alignItems="center">
                <Grid item xs={10} sm={6} md={4} lg={3}>
                    <Fade in timeout={1000}>
                        <img src={aboutme.profilePic} alt={aboutme.title} className={classes.profilePic}/>
                    </Fade>
                </Grid>
                <Grid item sm={12} md={8} lg={8}>
                    <h1>{aboutme.title}</h1>
                    <p className={classes.description}>{aboutme.description}</p>
                    <ColorButton href={aboutme.resume} target='_blank' className={classes.button}>Ver CV</ColorButton>
                </Grid>
            </Grid>
        </div>
    );
}

export default withStyles(styles)(Bio);