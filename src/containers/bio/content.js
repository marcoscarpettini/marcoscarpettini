import ProfilePic from '../../images/ProfilePic.jpg';
import CV from '../../files/MarcoScarpettini_CV.pdf';

const aboutme = {
    profilePic: ProfilePic,
    title: '¿Quién soy?',
    description: 'Mi nombre es Marco Scarpettini. Soy técnico electrónico egresado de la Escuela Técnica Roberto Rocca, estoy estudiando la "Licenciatura en Ciencias Biológicas" en la Facultad de Ciencias Exactas y Naturales de la UBA. Mis habilidades principales son la programación de microcontroladores, el desarrollo web, las matemáticas, las redes, el trabajo en equipo, el desarrollo de proyectos y la resolución de problemas. Estoy en búsqueda constante de nuevos desafíos que me permitan aprender cosas nuevas y perfeccionar mis habilidades, se puede decir que me apasionan los desafíos y el aprendizaje. ',
    resume: CV,
};

export default aboutme;