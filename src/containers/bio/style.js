const styles  = () => ({
    profilePic: {
        borderRadius: '50%',
        border: '.2rem solid #5ca4da',
    },
    button: {
        width: '7rem'
    },
    description: {
        textAlign: 'justify',
    },  
});

export default styles;