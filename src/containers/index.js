import Welcome from './welcome';
import Proyectos from './proyectos';
import Bio from './bio';

export {
    Welcome,
    Proyectos,
    Bio,
};