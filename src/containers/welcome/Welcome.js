import React from 'react';
import Fade from '@material-ui/core/Fade';
import { withStyles } from '@material-ui/styles';
import { Random } from 'react-animated-text';

import styles from './style'
import content from './content';

const { tagline, title, subtitle} = content;

function Welcome(props) {
  const {classes} = props;

  return (
    <div className={classes.container}>
      <h3 className={classes.headSubTitle}>
        {tagline}
      </h3>
      <h1 className={classes.title}>
          <Random
            text={title}
            iterations={1}
            effect="verticalFadeIn"
            effectDirection="up"
            effectChange={10.0}
          />
      </h1>
      <Fade in timeout={3000}>
        <h3>
          {subtitle}
        </h3>
      </Fade>
    </div>
  );
};

export default withStyles(styles)(Welcome);