const styles  = () => ({
    
    headSubTitle: {
        marginBottom: '.5rem',
    },
    title: {
        fontWeight: 700, 
        fontSize: '4.375rem',
        color: '#5ca4da',
        marginBottom: '1rem',
        '@media (max-width: 959.95px)' : {
            fontSize: '3.5rem', 
            marginBottom: '.5rem', 
        },
        '@media (max-width: 599.95px)' : {
            fontSize: '2.5rem',
            marginBottom: '.5rem',
        },
        '@media (max-width: 433.95px)' : {
            fontSize: '2rem',
            marginBottom: '.5rem',
        },
        '@media (max-width: 361.95px)' : {
            fontSize: '1.7rem',
            marginBottom: '.5rem',
        },
    },
    br: {
        display: 'none',
        '@media (max-width: 599.95px)' : {
            display: 'all',
        },
    },
    container: {
        paddingLeft: '3rem',
        '@media (max-width: 959.95px)' : {
            paddingLeft: '0rem', 
        },
    },
});
  
export default styles;