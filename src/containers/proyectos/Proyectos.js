import React from 'react';
import Grid from '@material-ui/core/Grid';
import Fade from 'react-reveal/Fade';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fab } from '@fortawesome/free-brands-svg-icons';

import styles from './style';
import proyectos from './content';

library.add(fab);

const ColorButton = withStyles(() => ({
    root: {
      color: '#ffffff',
      backgroundColor: '#6daee0',
      '&:hover': {
        backgroundColor: '#5ca4da',
      },
    },
}))(Button);

function Proyectos(props) {
    const {classes} = props;

    return(
        <div className={classes.container}>
            {proyectos.map((link, i) => (
                <Grid container spacing={5} key={i} justify='center' alignItems="center">
                    <Grid item sm={12} lg={4} container direction='column' justify='center'>
                        <Fade
                            left
                            duration={1000}
                            delay={500}
                            distance="30px"
                        > 
                            <Grid item xs>
                                <h2>{link.title}</h2>
                                <p className={classes.description}>{link.description}</p>
                            </Grid>
                            <Grid item xs direction='row' spacing={1} container  justify='space-evenly'>
                                <Grid item xs>
                                    <ColorButton href={link.url} target='_blank'>Ver página</ColorButton>
                                </Grid>
                                <Grid item xs>
                                    <Button variant='outlined' href={link.urlGIT} style={{color:'#5ca4da'}} target='_blank'><FontAwesomeIcon icon={['fab', 'gitlab']} />&nbsp;Ver GitLab</Button>
                                </Grid>
                            </Grid>
                        </Fade>
                    </Grid>
                    <Grid item sm={12} lg={8}>
                        <Fade
                            right
                            duration={1000}
                            delay={1000}
                            distance="30px"
                        >
                            <img src={link.img} alt={link.title} className={classes.image}/>
                        </Fade>
                    </Grid>
                </Grid>
            ))}
        </div>
    );
};

export default withStyles(styles)(Proyectos);