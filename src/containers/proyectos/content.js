import Puckie from '../../images/puckie.jpg';
import SGPuckie from '../../images/sg-puckie.jpg';
import CEAELM from '../../images/conelalmaenlasmanos.jpg';
import Portfolio from '../../images/marcoscarpettini.jpg';
import GuruChubasco from '../../images/guruchubasco.jpg';

const proyectos = [
    {   title:'Guru Chubasco', 
        description:'Página de pronostico del tiempo y visualización de las distintas variables metereológicas de cualquier lugar del mundo. El objetivo de la web es que sirva para poder ver el pronóstico metereológico de cualquier lugar del globo de una forma completa y de fácil lectura, se puede ver el pronóstico actual junto con un ampliado hora a hora de 24 horas y el pronostico a 7 días. Además la web se implemento como PWA. El desarrollo fue hecho con HTML, CSS, JS, ReactJS, Gatsby, Material-UI y Open Weather One Call API.', 
        urlGIT:'https://gitlab.com/marcoscarpettini/marcoscarpettini.git', 
        url:'https://guruchubasco.ga', 
        img: GuruChubasco },
    {   title:'Portfolio personal', 
        description:'Página de presentación y portfolio de desarrollos donde pueden encontrar información acerca de mí, de mis trabajos y de contacto. El objetivo de la web es que sirva de plataforma de promoción mía mostrando lo que hago, información sobre mí y de contacto. Además la web se implemento como PWA. El desarrollo fue hecho con HTML, CSS, JS, ReactJS, Gatsby y Material-UI.', 
        urlGIT:'https://gitlab.com/marcoscarpettini/marcoscarpettini.git', 
        url:'https://marcoscarpettini.ml', 
        img: Portfolio },
    {   title:'Con el alma en las manos', 
        description:'Página de promoción de cursos, talleres y terapias alternativas. El objetivo de la web es que se visibilice el centro holistico y se puedan cargar los distintos cursos, talleres y terapias que se van a estar dando, junto con toda la información pertinente (precio, fechas y horarios, descripción y una imagen). La página cuenta con una ventana principal donde se muestra toda la información sobre el centro y la información cargada, una ventana con un formulario de contacto y una ventana para ingresar al sistema de carga. El desarrollo fue hecho con HTML, CSS, JS, Bootstrap y Firebase Database, Storage y Authentication.', 
        urlGIT:'https://gitlab.com/marcoscarpettini/centro_holistico.git', 
        url:'https://conelalmaenlasmanos.netlify.app', 
        img: CEAELM },
    {   title:'Sistema de gestión Puckie', 
        description:'Página para gestión de stock y control de ventas de un emprendimiento. El objetivo de la web es que sirva de sistema de gestión para la compañía, la web cuenta con una sección de control de stock con los productos disponibles y los productos por realizar, tiene un campo para cargar la entrada de materia prima y otro para cargar la producción. Otra de las secciones es la de control de ventas que cuenta con la información de los productos vendidos (comprador, tipo de producto, monto pagado y vendedor), las ventas registradas actualizan automáticamente el stock. La última sección es la de gestión de usuarios donde se implemento un sistema de jerarquías para que no cualquiera pueda modificar los datos. Además la web se implemento como PWA con el fin de que sea más accesible para los usuarios. El desarrollo fue hecho con HTML, CSS, JS, Bootstrap y Firebase Database y Authentication.', 
        urlGIT:'https://gitlab.com/marcoscarpettini/sg-puckie.git', 
        url:'https://sg.puckie.com.ar', 
        img: SGPuckie },
    {   title:'Puckie', 
        description:'Página de promoción de un emprendimiento de juegos de mesa. El objetivo de la web es que sirva de plataforma de marketing y visibilización del emprendimiento, consta de información correspondiente a la empresa, como, productos, fines sociales y ambientales, compra de los productos y como está integrada, además, cuenta con un formulario de contacto. El desarrollo fue hecho con HTML, CSS, JS y Bootstrap.', 
        urlGIT:'https://gitlab.com/marcoscarpettini/puckie.git', 
        url:'https://puckie.com.ar', 
        img: Puckie },
];

export default proyectos;