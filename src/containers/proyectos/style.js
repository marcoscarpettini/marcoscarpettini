const styles  = () => ({
    description: {
        textAlign: 'justify',
    },
    image: {
        textAlign: 'center',
        maxHeight: '100vh',
        boxShadow: '0px 5px 10px rgba(0, 0, 0, 0.5)',
    },
    container: {
        padding: '1rem 0rem 3rem 3rem',
        '@media (max-width: 959.95px)' : {
            paddingLeft: '0rem', 
        },
    },
});

export default styles;