import React from "react";
import { Layout } from '../components';
import { SEO } from '../components';
import { Proyectos } from '../containers';

const PortfolioPage = () => (
    <Layout>
      <SEO title="Portfolio"/>
      <Proyectos/>
    </Layout>
  );
  
  export default PortfolioPage;