import React from "react";
import { Welcome } from '../containers';
import { Layout } from '../components';
import { SEO } from '../components';

const IndexPage = () => (
  <Layout>
    <SEO title="Inicio"/>
    <Welcome />
  </Layout>
);

export default IndexPage;