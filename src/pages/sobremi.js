import React from "react";
import { Layout } from '../components';
import { SEO } from '../components';
import { Bio } from '../containers';

const SobremiPage = () => (
    <Layout>
      <SEO title="Sobre mí"/>
      <Bio />
    </Layout>
  );
  
  export default SobremiPage;