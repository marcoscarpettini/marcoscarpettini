module.exports = {
  siteMetadata: {
    title: "Marco Scarpettini",
  },
  plugins: [
    `gatsby-plugin-netlify`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages/`,
      },
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Marco Scarpettini Portfolio`,
        short_name: `Marco Scarpettini`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#5ca4da`,
        display: `standalone`,
        description: `Esta es una página de presentación y portfolio de desarrollos donde pueden encontrar información acerca de mí, de mis trabajos y de contacto.`,
        lang: `es`,
        icon: `src/images/letra-m.png`, // This path is relative to the root of the site.
        icons: [
          {
            src: `/icons/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/icons/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
          {
            src: '/icons/maskable_iconx192',
            sizes: '192x192',
            type: 'image/png',
            purpose: 'any maskable',
          },
          {
            src: '/icons/maskable_iconx512',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'any maskable',
          }
        ], // Add or remove icon sizes as desired
      },
    },
    {  
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: [`/index/`, `/sobremi/`, `/portfolio/`, `/404/`],
      },
    },
  ],
};
